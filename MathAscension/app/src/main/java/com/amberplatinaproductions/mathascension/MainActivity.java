/*
 * Copyright (c) 2015. Matthew Hwang
 */

package com.amberplatinaproductions.mathascension;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity  {

    private String[] buttons = {"Addition","Subtraction","Multiplication","Division","Random","Records"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       // Button plus = (Button) findViewById(R.id.button);
        //Button minus = (Button) findViewById(R.id.button2);
       // Button mult = (Button) findViewById(R.id.button3);
       // Button div = (Button) findViewById(R.id.button4);
       // Button rand = (Button) findViewById(R.id.button5);



        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.activity_main,R.id.textView1,buttons);
        ListView list = (ListView) this.findViewById(R.id.buttonsList);
        list.setAdapter(adapter);
        list.setTextFilterEnabled(true);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String clicked = (String) parent.getAdapter().getItem(position);

                Toast.makeText(getApplicationContext(),clicked + " pushed",
                        Toast.LENGTH_SHORT).show();

                startWindow(clicked);

                /**
                //addition
                if(clicked.equalsIgnoreCase(buttons[0]))
                {
                    startWindow(0);

                }
                //subtraction
                else if(clicked.equalsIgnoreCase(buttons[1]))
                {
                    startWindow(1);
                }
                //multiplication
                else if(clicked.equalsIgnoreCase(buttons[2]))
                {
                    startWindow(2);
                }
                //division
                else if(clicked.equalsIgnoreCase(buttons[3]))
                {
                    startWindow(3);
                }
                //random
                else if(clicked.equalsIgnoreCase(buttons[4]))
                {
                    startWindow(4);
                }
                //records
                else if(clicked.equalsIgnoreCase(buttons[5]))
                {
                    startWindow(0);
                }

                startWindow(0);
                **/
            }
        });





    }

    public void startWindow(String mode)
    {
        Intent i = new Intent(this.getApplicationContext(), Window.class);
        i.putExtra("key",mode);
        this.startActivity(i);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
