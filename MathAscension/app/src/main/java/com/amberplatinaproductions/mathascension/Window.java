/*
 * Copyright (c) 2015. Matthew Hwang
 */

package com.amberplatinaproductions.mathascension;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class Window extends ActionBarActivity implements View.OnClickListener{

    private String equation = "";
    private String input = "";
    private int answer = 0;
   private TextView equationLine;
    private TextView inputLine;
    private TextView messageLine;
    private String gameMode;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_window);




       // TextView tv= (TextView)findViewById(R.id.windowText);
        //tv.setBackgroundResource( R.drawable.textview_border);

        equationLine = (TextView) findViewById(R.id.questionText);
        inputLine = (TextView) findViewById(R.id.inputText);
        messageLine = (TextView) findViewById(R.id.messageText);

        Bundle mode = getIntent().getExtras();
        if (mode != null) {
            gameMode = mode.getString("key");
            messageLine.setText(gameMode);
        }



        //sets listeners for number pad buttons
        //needs listener for enter, and delete buttons later
        Button oneButton = (Button) findViewById(R.id.one_button);
        oneButton.setOnClickListener(this);
        Button twoButton = (Button) findViewById(R.id.two_button);
        twoButton.setOnClickListener(this);
        Button threeButton = (Button) findViewById(R.id.three_button);
        threeButton.setOnClickListener(this);
        Button fourButton = (Button) findViewById(R.id.four_button);
        fourButton.setOnClickListener(this);
        Button fiveButton = (Button) findViewById(R.id.five_button);
        fiveButton.setOnClickListener(this);
        Button sixButton = (Button) findViewById(R.id.six_button);
        sixButton.setOnClickListener(this);
        Button sevenButton = (Button) findViewById(R.id.seven_button);
        sevenButton.setOnClickListener(this);
        Button eightButton = (Button) findViewById(R.id.eight_button);
        eightButton.setOnClickListener(this);
        Button nineButton = (Button) findViewById(R.id.nine_button);
        nineButton.setOnClickListener(this);
        Button zeroButton = (Button) findViewById(R.id.zero_button);
        zeroButton.setOnClickListener(this);
        Button enterButton = (Button) findViewById(R.id.enter_button);
        enterButton.setOnClickListener(this);


        questionGenerator(0,0);



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_window, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        Toast.makeText(getApplicationContext(),  " pushed",
                Toast.LENGTH_SHORT).show();

        switch(v.getId()) {

            case R.id.one_button:
                input = input + "1";
                break;
            case R.id.two_button:
               input = input + "2";
                break;
            case R.id.three_button:
                input = input + "3";
                break;
            case R.id.four_button:
                input = input + "4";
                break;
            case R.id.five_button:
                input = input + "5";
                break;
            case R.id.six_button:
                input = input + "6";
                break;
            case R.id.seven_button:
                input = input + "7";
                break;
            case R.id.eight_button:
                input = input + "8";
                break;
            case R.id.nine_button:
                input = input + "9";
                break;
            case R.id.zero_button:
                input = input + "0";
                break;
            case R.id.enter_button:
                checkAnswer();
                break;

        }

        inputLine.setText(input);
    }

    /**
     * Creates an arithmetic question based on parameters given.
     * @param sign 0 for plus, 1 for minus, 2 for multiplication, 3 for division, 4 for random
     * @param difficulty 1 to 10. formula TBD
     */
    public void questionGenerator(int sign, int difficulty)
    {
        Double leftNumber1 = Math.random() * 10;
        Double rightNumber1 = Math.random() * 10;

        int leftNumber2 = leftNumber1.intValue();
        int rightNumber2 = rightNumber1.intValue();
        String text = "";
        if(gameMode.equalsIgnoreCase("Addition"))
        {
            text = leftNumber2 + " + " + rightNumber2;
            answer = leftNumber2 + rightNumber2;
        }
        else if(gameMode.equalsIgnoreCase("subtraction"))
        {
            text = leftNumber2 + " - " + rightNumber2;
            answer = leftNumber2 - rightNumber2;
        }
        else if(gameMode.equalsIgnoreCase("multiplication"))
        {
            text = leftNumber2 + " x " + rightNumber2;
            answer = leftNumber2 * rightNumber2;
        }
        else if(gameMode.equalsIgnoreCase("division"))
        {
            text = (leftNumber2 * rightNumber2) + "\u00F7" + rightNumber2; //unicode for obelus/division operator
            answer = (leftNumber2 * rightNumber2) / rightNumber2;
        }

        equationLine.setText(text);


    }

    public void checkAnswer()
    {

    }
}
